﻿using ClosedXML.Excel;
using System;
using System.Data;
using System.IO;
using System.Collections.Specialized;
using TestingGround.lib;
using System.Diagnostics;

namespace TestingGround
{
    public class Testclass
    {
        public static void Main(string[] args)
        {
            //Declaring Variables
            //Ting Sen: I was learning to implement settings.ini in C# when I write this section. May not be optimal the way forward.
            string filePath = Properties.Settings.Default.file_path;
            string sheet = Properties.Settings.Default.sheet;
            StringCollection sc = Properties.Settings.Default.column_names;
            string[] columnNames = new string[sc.Count];
            sc.CopyTo(columnNames, 0);
            string destinationPath = @Properties.Settings.Default.result_file_name;

            #region Extracting Excel Tables
            //Uses TableExtractor Class to extract table. What the codes are doing is self-explanatory with console responses.
            Console.WriteLine("The test has started!");
            var stopWatch = Stopwatch.StartNew();
            Console.WriteLine("Reading Excel file......");
            XLWorkbook xmlFile = new XLWorkbook(filePath);
            Console.WriteLine("Reading completed! Elapsed time(ms): " + stopWatch.ElapsedMilliseconds);
            stopWatch.Reset();
            stopWatch.Start();
            Console.WriteLine("Table extraction is running......");
            DataTable dataTable = TableExtractor.ExtractTableFromExcel(xmlFile, sheet);
            Console.WriteLine("Table extraction completed! Elapsed time(ms): " + stopWatch.ElapsedMilliseconds);
            stopWatch.Reset();
            #endregion

            #region Adding Columns
            DataColumn StatusColumn = new DataColumn("Wrong Column Name", typeof(String));
            StatusColumn.DefaultValue = "new";
            dataTable.Columns.Add(StatusColumn);
            #endregion

            #region Renaming Columns
            dataTable.Columns["Wrong Column Name"].ColumnName = "Status";
            #endregion

            #region To CSV Test
            //stopWatch.Start();
            //Console.WriteLine("Converting to CSV file......");
            //string csvData = DataTableExtensions.ToCsv(dataTable);
            //File.WriteAllText(destinationPath, csvData);
            //Console.WriteLine("Conversion completed! Elapsed time(ms): " + stopWatch.ElapsedMilliseconds);
            #endregion

            #region To SQL Database Test
            //stopWatch.Start();
            //Console.WriteLine("Writing to SQL Database......");
            //string connectionString = "Server = MYKULASPL0136\\SQLEXPRESS; Database = test_asp_billing; Integrated Security = True;";
            //SqlExtensions sqlExtensions = new SqlExtensions(connectionString);
            //sqlExtensions.DatatablesToSqlDb(dataTable, "test_asp_billing", "ES_C&B_Indonesia");
            //Console.WriteLine("Writing completed! Elapsed time(ms): " + stopWatch.ElapsedMilliseconds);
            //stopWatch.Reset();
            #endregion

            Console.WriteLine("Press ENTER to close console......");
            ConsoleKeyInfo keyInfo = Console.ReadKey();
            while (keyInfo.Key != ConsoleKey.Enter)
                keyInfo = Console.ReadKey();
        }
    }
}