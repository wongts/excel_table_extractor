﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace TestingGround.lib
{
    public class SqlExtensions
    {
        private string ConnectionString { get; set; }

        public SqlExtensions() { }

        public SqlExtensions(string strConnectionString) => this.ConnectionString = strConnectionString;

        public void DatatablesToSqlDb(DataTable table, string dbName, string dbTableName)
        {

            using (SqlConnection connection = new SqlConnection(this.ConnectionString))
            {
                connection.Open();

                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                {
                    bulkCopy.DestinationTableName = "[" + dbName + "].[dbo].[" + dbTableName + "]";

                    MapColumns(table, bulkCopy);
                    try
                    {
                        //Write from the source to the destination.
                        bulkCopy.WriteToServer(table);
                    }
                    catch (Exception ex)
                    {
                        //Log the exception somewhere & recover gracefully.
                        throw ex;
                    }
                }
            }
        }

        //Only works when datatable column names and database table column names match
        //Advised to manipulate the datatable such that the column names matches the target table in SQL database
        private static void MapColumns(DataTable infoTable, SqlBulkCopy bulkCopy)
        {
            try
            {
                foreach (DataColumn dc in infoTable.Columns)
                {
                    bulkCopy.ColumnMappings.Add(dc.ColumnName, dc.ColumnName);
                }
            }
            catch (Exception ex)
            {
                //Log the exception somewhere & recover gracefully.
                throw ex;
            }
        }
    }
}
