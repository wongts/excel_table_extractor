﻿using ClosedXML.Excel;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace TestingGround.lib
{
    public static class TableExtractor
    {
        //Extract specific columns
        public static DataTable ExtractTableFromExcel(XLWorkbook xmlFile, string sheet, string[] columnNames)
        {
            var workSheet = xmlFile.Worksheet(sheet);
            if (workSheet.AutoFilter != null)
            {
                workSheet.AutoFilter.Clear();
            }

            //Get these variables to identify a rectangle-shaped area which data exists
            var firstRowNumber = workSheet.FirstRowUsed().RowNumber();
            var firstColumnNumber = workSheet.FirstColumnUsed().ColumnNumber();
            var lastRowNumber = workSheet.LastRowUsed().RowNumber();
            var lastColumnNumber = workSheet.LastColumnUsed().ColumnNumber();

            // Get a range with the remainder of the worksheet data (the range used)
            var range = workSheet.Range(firstRowNumber, firstColumnNumber, lastRowNumber, lastColumnNumber).AsRange();

            // Treat the range as a table (to be able to use the column names)
            var table = range.AsTable();

            //Specify what are all the Columns you need to get from Excel
            var columns = new List<string[]>();

            //Looping through columns by columns, appending data in columns to an array which is then appended to a list
            for (int i = 0; i < columnNames.Length; i++)
            {
                columns.Add(table.DataRange
                    .Rows()
                    .Select(tableRow => tableRow
                        .Field(columnNames[i])
                        .CachedValue?
                        .ToString()
                    )
                    .ToArray()
                );
            }

            //Convert List of arrays to DataTable
            DataTable dataTable = ConvertListToDataTable(columns, columnNames);
            return dataTable;
        }

        //Extracts all columns
        public static DataTable ExtractTableFromExcel(XLWorkbook xmlFile, string sheet)
        {
            var workSheet = xmlFile.Worksheet(sheet);
            if (workSheet.AutoFilter != null)
            {
                workSheet.AutoFilter.Clear();
            }

            var firstRowNumber = workSheet.FirstRowUsed().RowNumber();
            var firstColumnNumber = workSheet.FirstColumnUsed().ColumnNumber();
            var lastRowNumber = workSheet.LastRowUsed().RowNumber();
            var lastColumnNumber = workSheet.LastColumnUsed().ColumnNumber();

            var range = workSheet.Range(firstRowNumber, firstColumnNumber, lastRowNumber, lastColumnNumber).AsRange();

            var table = range.AsTable();

            var columns = new List<string[]>();

            //Declare the variable to store column headers extracted from table
            var columnNames = new List<string>();
            for (int i = 0; i < lastColumnNumber - firstColumnNumber; i++)
            {
                columnNames.Add(table.HeadersRow().Cell(i+1).GetValue<string>());
                columns.Add(table.DataRange
                    .Rows()
                    .Select(tableRow => tableRow
                        .Field(i)
                        .CachedValue?
                        .ToString()
                    )
                    .ToArray()
                );
            }

            //Convert List to DataTable
            DataTable dataTable = ConvertListToDataTable(columns, columnNames.ToArray());
            return dataTable;
        }

        private static DataTable ConvertListToDataTable(IReadOnlyList<string[]> columns, string[] columnNames)
        {
            var table = new DataTable("TestTable");
            var rows = columns.Select(array => array.Length).Concat(new[] { 0 }).Max();

            for (int i =0; i < columnNames.Length; i++)
            {
                table.Columns.Add(columnNames[i]);
            }

            for (var j = 0; j < rows; j++)
            {
                var row = table.NewRow();
                for (var k =0; k < columnNames.Length; k++)
                {
                    row[columnNames[k]] = columns[k][j];
                }
                table.Rows.Add(row);
            }
            return table;
        }
    }
}
